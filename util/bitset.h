// Copyright 2020 Kevin George <kevingeorgetm@gmail.com>.  All rights reserved.

#ifndef UTIL_BITSET_H_
#define UTIL_BITSET_H_

#include <algorithm>
#include <bitset>
#include <cstdint>
#include <iostream>
#include <stdexcept>
#include <utility>

#include "util/error.h"

namespace util {

constexpr uint8_t _UTIL_BITS_IN_BYTE = 8;

// NOLINTNEXTLINE(runtime/int): __builtin_clzll takes unsigned long long
constexpr std::size_t _ctz(unsigned long long n) noexcept {
  // TODO(kilogram): make work without clang/GCC
  return n == 0 ? sizeof(n) * _UTIL_BITS_IN_BYTE : __builtin_ctzll(n);
}

// NOLINTNEXTLINE(runtime/int): __builtin_clzll takes unsigned long long
constexpr std::size_t _clz(unsigned long long n) noexcept {
  // TODO(kilogram): make work without clang/GCC
  return n == 0 ? sizeof(n) * _UTIL_BITS_IN_BYTE : __builtin_clzll(n);
}

template <std::size_t N>
class bitset;
template <std::size_t N>
bitset<N> operator&(const bitset<N> &lhs, const bitset<N> &rhs) noexcept;
template <std::size_t N>
bitset<N> operator|(const bitset<N> &lhs, const bitset<N> &rhs) noexcept;
template <std::size_t N>
bitset<N> operator^(const bitset<N> &lhs, const bitset<N> &rhs) noexcept;
template <std::size_t N>
std::ostream &operator<<(std::ostream &os, const bitset<N> &rhs);

template <std::size_t _N>
class bitset {
 public:
  static constexpr std::size_t N = _N;
  static_assert(N > 0);

  typedef decltype(std::bitset<N>().to_ullong()) _word;
  static constexpr std::size_t _sz = _UTIL_BITS_IN_BYTE * sizeof(_word);
  static constexpr std::size_t COUNT = ((N - 1) / _sz) + 1;

  typedef std::bitset<_sz> value_type;

  constexpr bitset() noexcept : bitset(0) {}
  constexpr explicit bitset(_word val) noexcept : data({val}) {}

  class reference {
   public:
    reference &operator=(bool x) noexcept {
      in_word = x;
      return *this;
    }
    reference &operator=(const reference &x) noexcept {
      in_word = x;
      return *this;
    }
    operator bool() const noexcept { return static_cast<bool>(in_word); }
    bool operator~() const noexcept { return ~in_word; }
    reference &flip() noexcept {
      in_word = in_word.flip();
      return *this;
    }

   private:
    reference(bitset<N> *bitset, std::size_t pos)
        : in_word(bitset->data[pos / _sz][pos % _sz]) {}

    typename value_type::reference in_word;

    friend class bitset;
  };

  template <std::size_t offset, std::size_t width>
  class projection {
   private:
    explicit projection(bitset<N> *bitset) : b(bitset) {}

    bitset<N> &b;

    friend class bitset;
  };

#define FOREACH(b, i) for (std::size_t i = 0; i < (b).COUNT; i++)

  explicit operator std::bitset<N>() const noexcept {
    std::bitset<N> ret;
    for (int64_t i = COUNT - 1; i >= 0; i--)
      ret = (ret << _sz) | std::bitset<N>(data[i].to_ullong());
    return ret;
  }

  bool operator==(const bitset<N> &rhs) const noexcept {
    FOREACH(*this, i) if (data[i] != rhs.data[i]) return false;
    return true;
  }
  bool operator!=(const bitset<N> &rhs) const noexcept {
    return !(*this == rhs);
  }

  bool operator==(const std::bitset<N> &rhs) const noexcept {
    return static_cast<const std::bitset<N>>(*this) == rhs;
  }
  bool operator!=(const std::bitset<N> &rhs) const noexcept {
    return !(*this == rhs);
  }

  constexpr bool operator[](std::size_t pos) const {
    return data[pos / _sz][pos % _sz];
  }
  reference operator[](std::size_t pos) { return reference(this, pos); }

  bool test(std::size_t pos) const {
    if (pos >= N) {
      THROW_OR_ABORT(std::out_of_range(
          "bitset: position (which is %zu) > size() (which is %zu)"));
    }
    return (*this)[pos];
  }

  bool all() const noexcept {
    FOREACH(*this, i) if (!data[i].all()) return false;
    return true;
  }

  bool any() const noexcept {
    FOREACH(*this, i) if (data[i].any()) return true;
    return false;
  }

  bool none() const noexcept { return !any(); }

  std::size_t count() const noexcept {
    std::size_t c = 0;
    FOREACH(*this, i) c += data[i].count();
    return c;
  }

  std::size_t clz() const noexcept {
    std::size_t c = 0, val = _sz;
    for (int64_t i = COUNT - 1; val >= _sz && i >= 0; i--)
      c += (val = _clz(data[i].to_ullong()));
    return (N % _sz == 0) ? c : c - _sz + (N % _sz);
  }

  std::size_t ctz(std::size_t offset) const noexcept {
    std::size_t word = offset / _sz, bit = offset % _sz;
    if (offset >= N || word >= COUNT) return 0;

    _word first_word = data[word].to_ullong();
    _word mask = (bit == 0) ? 0x0 : (1ULL << (bit)) - 1;

    std::size_t val = std::min(_ctz(first_word & ~mask), N - (word * _sz));
    std::size_t c = val - bit;

    for (std::size_t i = word + 1; val >= _sz && i < COUNT; ++i)
      c += (val = std::min(_ctz(data[i].to_ullong()), N - (i * _sz)));

    return c;
  }

  constexpr std::size_t size() const noexcept { return N; }

  bitset<N> &operator&=(const bitset<N> &other) noexcept {
    FOREACH(*this, i) data[i] &= other.data[i];
    return *this;
  }
  bitset<N> &operator|=(const bitset<N> &other) noexcept {
    FOREACH(*this, i) data[i] |= other.data[i];
    return *this;
  }
  bitset<N> &operator^=(const bitset<N> &other) noexcept {
    FOREACH(*this, i) data[i] ^= other.data[i];
    return *this;
  }
  bitset<N> operator~() const noexcept {
    bitset<N> ret;
    FOREACH(*this, i) ret.data[i] = ~data[i];
    return ret;
  }

  // bitset<N> operator<<(std::size_t pos) const noexcept;
  // bitset<N>& operator<<=(std::size_t pos) noexcept;
  // bitset<N> operator>>(std::size_t pos) const noexcept;
  // bitset<N>& operator>>=(std::size_t pos) noexcept;

  bitset<N> &set() noexcept {
    FOREACH(*this, i) data[i].set();
    return *this;
  }
  bitset<N> &set(std::size_t pos, bool value = true) {
    reference(this, pos) = value;
    return *this;
  }

  bitset<N> &reset() noexcept {
    FOREACH(*this, i) data[i].reset();
    return *this;
  }
  bitset<N> &reset(std::size_t pos) {
    reference(this, pos) = false;
    return *this;
  }

  bitset<N> &flip() noexcept {
    FOREACH(*this, i) data[i].flip();
    return *this;
  }
  bitset<N> &flip(std::size_t pos) {
    reference(this, pos).flip();
    return *this;
  }

  friend bitset<N> operator&<N>(const bitset<N> &lhs,
                                const bitset<N> &rhs) noexcept;
  friend bitset<N> operator|
      <N>(const bitset<N> &lhs, const bitset<N> &rhs) noexcept;
  friend bitset<N> operator^
      <N>(const bitset<N> &lhs, const bitset<N> &rhs) noexcept;
  friend std::ostream &operator<<<N>(std::ostream &os, const bitset<N> &rhs);

 private:
  value_type data[COUNT];
};

template <std::size_t N>
bitset<N> operator&(const bitset<N> &lhs, const bitset<N> &rhs) noexcept {
  bitset<N> ret;
  FOREACH(lhs, i) ret.data[i] = lhs.data[i] & rhs.data[i];
  return ret;
}
template <std::size_t N>
bitset<N> operator|(const bitset<N> &lhs, const bitset<N> &rhs) noexcept {
  bitset<N> ret;
  FOREACH(lhs, i) ret.data[i] = lhs.data[i] | rhs.data[i];
  return ret;
}
template <std::size_t N>
bitset<N> operator^(const bitset<N> &lhs, const bitset<N> &rhs) noexcept {
  bitset<N> ret;
  FOREACH(lhs, i) ret.data[i] = lhs.data[i] ^ rhs.data[i];
  return ret;
}

template <std::size_t N>
std::ostream &operator<<(std::ostream &os, const bitset<N> &rhs) {
  os << std::bitset<(N % rhs._sz == 0 ? N : N % rhs._sz)>(
      rhs.data[rhs.COUNT - 1].to_ullong());
  for (int64_t i = rhs.COUNT - 2; i >= 0; i--) os << rhs.data[i];
  return os;
}

#ifdef FOREACH
#undef FOREACH
#endif

}  // namespace util

#endif  // UTIL_BITSET_H_
