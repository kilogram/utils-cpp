// Copyright 2020 Kevin George <kevingeorgetm@gmail.com>.  All rights reserved.

#include "util/bitset.h"

#include <cstdint>

#include "gtest/gtest.h"

namespace {

using ::util::bitset;

TEST(BitTest, BitTest) {
  bitset<1> bit;
  EXPECT_FALSE(bit.test(0));

  bit.set(0);
  EXPECT_TRUE(bit.test(0));

  bit.reset(0);
  EXPECT_FALSE(bit[0]);

  EXPECT_THROW(bit.test(1), std::out_of_range);
}

template <class T>
class BitsetTest : public ::testing::Test {};

typedef testing::Types<bitset<4>, bitset<31>, bitset<32>, bitset<33>,
                       bitset<63>, bitset<64>, bitset<65>, bitset<1000>,
                       bitset<1023>, bitset<1024>, bitset<1025>>
    bitsetSizes;
TYPED_TEST_SUITE(BitsetTest, bitsetSizes);

TYPED_TEST(BitsetTest, TestSetFlipResetTest) {
  std::bitset<TypeParam::N> a_std, b_std, c_std;
  TypeParam a_new, b_new, c_new;

  for (std::size_t i = 0; i < TypeParam::N; i += 7) {
    a_new.set(i);
    a_std.set(i);
  }
  EXPECT_EQ(a_new, a_std);

  a_new.set(2, 1);
  a_std.set(2, 1);
  EXPECT_EQ(a_new, a_std);

  a_new.set(2, 0);
  a_std.set(2, 0);
  EXPECT_EQ(a_new, a_std);

  for (std::size_t i = 0; i < TypeParam::N; i += 7) {
    EXPECT_EQ(a_new.test(i), a_std.test(i));
    a_new.reset(i);
    a_std.reset(i);
  }
  EXPECT_EQ(a_new, a_std);

  a_new.flip();
  a_std.flip();
  EXPECT_EQ(a_new, a_std);

  EXPECT_THROW(a_new.test(TypeParam::N), std::out_of_range);
}

TYPED_TEST(BitsetTest, TestComparators) {
  TypeParam a, b, c;

  a.set(1);
  b.set(2);

  EXPECT_NE(a, b);

  a.reset(1);
  EXPECT_EQ(a, c);

  a.set(2);
  EXPECT_EQ(a, b);
}

TYPED_TEST(BitsetTest, TestArithOps) {
  std::bitset<TypeParam::N> a_std, b_std, c_std;
  TypeParam a_new, b_new, c_new;

  a_new.set(1);
  a_std.set(1);
  b_new.set(2);
  b_std.set(2);
  c_new = a_new | b_new;
  c_std = a_std | b_std;
  EXPECT_EQ(c_new, c_std);

  a_new.set(2);
  a_std.set(2);
  c_new = a_new & b_new;
  c_std = a_std & b_std;
  EXPECT_EQ(c_new, c_std);

  a_new.reset();
  a_std.reset();
  c_new = a_new & b_new;
  c_std = a_std & b_std;
  EXPECT_EQ(c_new, c_std);

  a_new.flip();
  a_std.flip();
  c_new = b_new ^ a_new;
  c_std = b_std ^ a_std;
  EXPECT_EQ(c_new, c_std);

  a_new.reset();
  a_std.reset();
  for (std::size_t i = 0; i < TypeParam::N; i += 7) {
    a_new.set(i);
    a_std.set(i);
  }
  EXPECT_EQ(a_new, a_std);

  c_new |= a_new;
  c_std |= a_std;
  EXPECT_EQ(c_new, c_std);

  c_new &= a_new;
  c_std &= a_std;
  EXPECT_EQ(c_new, c_std);
  EXPECT_EQ(c_new, a_new);

  c_new.flip();
  c_std.flip();
  c_new ^= a_new;
  c_std ^= a_std;
  EXPECT_EQ(c_new, c_std);
  EXPECT_EQ(c_new, ~std::bitset<TypeParam::N>());

  a_new = ~c_new;
  a_std = ~c_std;
  EXPECT_EQ(a_new, a_std);
  EXPECT_EQ(a_new, std::bitset<TypeParam::N>());
}

TYPED_TEST(BitsetTest, TestReferenceOps) {
  constexpr std::size_t N = TypeParam::N;

  TypeParam a_new;

  for (std::size_t i = 0; i < N; i += 7) {
    a_new[i] = 1;
  }

  for (std::size_t i = 0; i < N; ++i) {
    EXPECT_EQ(a_new[i], i % 7 == 0);
  }

  for (std::size_t i = 0; i < N; i += 7) {
    a_new[i].flip();
  }

  EXPECT_EQ(a_new, std::bitset<N>());
  a_new[0] = 1;
  a_new[1] = a_new[0];
  EXPECT_TRUE(a_new.test(0));
  EXPECT_TRUE(a_new.test(1));
}

TYPED_TEST(BitsetTest, TestAllAnyNone) {
  TypeParam a_new;

  EXPECT_TRUE(a_new.none());

  auto idx = TypeParam::N / 2;
  a_new[idx] = 1;
  EXPECT_TRUE(a_new.any());
  EXPECT_FALSE(a_new.none());

  a_new.flip();
  EXPECT_TRUE(a_new.any());
  EXPECT_FALSE(a_new.none());
  EXPECT_FALSE(a_new.all());

  a_new[idx] = 1;
  EXPECT_TRUE(a_new.any());
  EXPECT_FALSE(a_new.none());
  EXPECT_TRUE(a_new.all());

  a_new = ~a_new;
  EXPECT_FALSE(a_new.any());
  EXPECT_TRUE(a_new.none());
  EXPECT_FALSE(a_new.all());
}

TYPED_TEST(BitsetTest, TestClz) {
  constexpr std::size_t N = TypeParam::N;

  TypeParam a_new;
  EXPECT_EQ(a_new.clz(), N);

  a_new.set(0);
  EXPECT_EQ(a_new.clz(), N - 1);

  a_new.set(1);
  EXPECT_EQ(a_new.clz(), N - 2);

  auto idx = N / 2;
  a_new.set(idx);
  EXPECT_EQ(a_new.clz(), N - idx - 1);

  a_new.set(N - 2);
  EXPECT_EQ(a_new.clz(), 1);

  a_new.set(N - 1);
  EXPECT_EQ(a_new.clz(), 0);
}

TYPED_TEST(BitsetTest, TestCtz) {
  constexpr std::size_t N = TypeParam::N;

  TypeParam a_new;
  EXPECT_EQ(a_new.ctz(0), N);
  EXPECT_EQ(a_new.ctz(N - 1), 1);

  a_new.set(N - 1);
  EXPECT_EQ(a_new.ctz(0), N - 1);
  EXPECT_EQ(a_new.ctz(1), N - 2);

  auto idx = N / 2;
  a_new.reset();
  a_new.set(idx);
  auto before = N - idx - 1;
  EXPECT_EQ(a_new.ctz(0), idx);
  EXPECT_EQ(a_new.ctz(1), idx - 1);
  EXPECT_EQ(a_new.ctz(2), idx - 2);
  EXPECT_EQ(a_new.ctz(idx - 1), 1);
  EXPECT_EQ(a_new.ctz(idx), 0);
  EXPECT_EQ(a_new.ctz(idx + 1), before);

  EXPECT_EQ(a_new.ctz(N), 0);
  EXPECT_EQ(a_new.ctz(N + 1), 0);
}

}  // namespace
