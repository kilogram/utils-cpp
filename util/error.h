// Copyright 2020 Kevin George <kevingeorgetm@gmail.com>.  All rights reserved.

#ifndef UTIL_ERROR_H_
#define UTIL_ERROR_H_

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <exception>
#include <type_traits>

template <typename T>
void _do_abort(const char* file, unsigned int line, const char* func,
               const char* msg) {
#if __cpp_exxeptions
  if (!std::is_same<T, void>::value) {
    throw T(msg);
    return;
  }
#endif
  fprintf(stderr, "In %s (at %s:%d): ", func, file, line);
  fputs(msg, stderr);
  fputs("\n", stderr);
  std::abort();
}

template <typename T>
void _abort(const char* file, unsigned int line, const char* func) {
  _do_abort<T>(file, line, func, "abort");
}

template <typename T>
void _abort(const char* file, unsigned int line, const char* func,
            const char* fmt...) {
  va_list args, args2;
  va_start(args, fmt);
  va_copy(args2, args);
  // NOLINTNEXTLINE(runtime/arrays): Prefer variable length arrays to alloca
  char msg[1 + vsnprintf(NULL, 0, fmt, args)];
  va_end(args);

  vsnprintf(msg, sizeof(msg), fmt, args2);
  va_end(args2);

  _do_abort<T>(file, line, func, msg);
}

#if __cpp_exceptions
#define THROW_OR_ABORT(_EXC) (throw(_EXC))
#else
#define THROW_OR_ABORT(_EXC) \
  (_do_abort<void>(__FILE__, __LINE__, __func__, _EXC.what()))
#endif

#define THROW_OR_ABORT_FMT(_EXC, ...) \
  _abort<_EXC>(__FILE__, __LINE__, __func__, __VA_ARGS__)

#define ABORT(...) \
  _abort<void>(__FILE__, __LINE__, __func__ __VA_OPT__(, ) __VA_ARGS__)

#define ASSERT(COND, ...)                                \
  if (!(COND)) {                                         \
    ABORT(__VA_OPT__(__VA_ARGS__, ) "assertion failed"); \
  }

#define UNREACHABLE(...) ABORT(__VA_OPT__(__VA_ARGS__, ) "unreachable")

#ifdef NDEBUG
#define DEBUG_ASSERT(...)
#define DEBUG_UNREACHABLE(...)
#else
#define DEBUG_ASSERT ASSERT
#define DEBUG_UNREACHABLE UNREACHABLE
#endif

#endif  // UTIL_ERROR_H_
